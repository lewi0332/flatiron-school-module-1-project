# Module 1 Project

## Flatiron School - NYC 

## Data Science Immersive - Jan 28th, 2019 Cohort

## Contributors - [Karen Lin](https://github.com/karenyilin) and [Derrick Lewis](https://github.com/lewi0332)

### 

The requirements of this project are to collect data either through API calls or webscraping, put them in tabular format, and create visualizations and exploratory analysis to tell a story of these data.

Karen Lin and Derrick Lewis collaborated on this project to collect data regarding Vegan vs. Steak Restaurants from 11 cities in the United States. The goal was to look for comparisons between these nearly opposite foods. We will look for restaturant density, price disparity and local sentiment through ratings. 

### Criteria
With some early tests directly on the Yelp website, we found more distinct results from a category label search rather than searching for term across the entire restaurant listing. This returns restaurants with specific tags for that category often intentionally placed by the restaurant owner.

### Search Area
To create a uniform comparison we decided to look at an exact 40 km radius starting from each urban center. We will pair this with population data from Freemaptools.com. Freemaptools.com creates a population estimate of a specific radius from a starting point.

![Map showing the 11 cities used in this exploratory analysis](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/Screen%20Shot%202019-02-15%20at%2010.02.53%20AM.png?raw=true)

To begin we will import data from the Yelp API. 

# The Function to pull from the API
We first built a request function. The restrictions on the Yelp API will limit our results to just 50 businesses. As most urban areas have more restaurants available we will need to create a loop to make multiple requests each time at a new starting point in the list. 



[FUNCTION TO COLLECT DATA.](https://gitlab.com/lewi0332/flatiron-school-module-1-project/wikis/API-Request-Function)

# Exploratory Data Analysis

Here we explore various views of our Yelp_Api data in order to discover insights about our two food types in each region.

We started with a look at the total counts of each type of restaurant across our 11 cities. 

![Plot of total number of restaurants](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/tot_count.png?raw=true)

We were initially surprised not only with the amount of vegan restaurants in our sample set, but that the vegan labeled restaurants slightly outnumbered the more traditional steakhouses. 

### By City

Next we looked at the totals by each city. We expected to see more steak restaurants in the Midwest region. This presented expected results with more cities such as Portland and Los Angeles showing significantly larger counts in vegan labeled restaurants. 

![plot of total counts of each category representing each 11 cities](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/city_counts.png?raw=true)

### By City Normalized

Our plot showed expected variance in the counts based on the size of our cities. We decided to look at these counts as the pertain to the population. This created a surprising view, with cities like Austin creating a significant change in their available vegan restaurants

![Restaurant counts by city per capita](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/city_capita.png?raw=true)

# How do these businesses compare? 

Next we explored some comparisons of these distinct businesses. Yelp provides us with a price category for each entry. We started with a comparison of the normal distribution of price between the two types of restaurants.

![normal distribution curve for each restaurant type](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/%20total_price.png?raw=true)

We were slightly unsurprised to see the restaurants in the Vegan category trend toward a lower price. We also noticed the wider distribution of steak restaurants, though this is doesn't yet provide insight. 

Next we compared the normal distributions of ratings for two types of restaurants. Both types of restaurants rated high, but with some surprise, vegan restaurants outperform the average rating of steakhouses. 

![Normal distribution of ratings for each category](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/total_ratings.png?raw=true)

Considering vegan restaurants trend cheaper, it was interesting to see greater likelihood for a higher rating.

### With a KDE Distribution
This helps us see the counts and spread of our distribution.

![Total price kde](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/total_price_kde.png?raw=true)

![rating kde](https://github.com/lewi0332/flatiron_mod_1_project/blob/master/plots/total_rating_kde.png?raw=true)

## These results were interesting and we decided to explore a few of the same plots for individual cities. 

[NORMAL DISTRIBUTIONS BY CITY](https://gitlab.com/lewi0332/flatiron-school-module-1-project/wikis/home)

# Testing Our Comparisons

We tested our two sample means with a Z-test score to gauge our confidence in each sample against the populations

### The Z score for the two sample means of the entire population is 14.795, with a P Value of 0.0.

* The Z score for portland is  6.491  and the P value is  0.0

* The Z score for los angeles is  7.581  and the P value is  0.0

* The Z score for new york is  4.047  and the P value is  0.0

* The Z score for boston is  4.454  and the P value is  0.0

* The Z score for chicago is  3.14  and the P value is  0.002

* The Z score for austin is  4.298  and the P value is  0.0

* The Z score for miami is  5.478  and the P value is  0.0

* The Z score for tulsa is  6.943  and the P value is  0.0

* The Z score for minneapolis is  3.144  and the P value is  0.002

* The Z score for san diego is  4.57  and the P value is  0.0

* The Z score for san francisco is  2.819  and the P value is  0.005

# Results

This project allowed us to practice and learn several skills in data analysis. Pulling and cleaning the data was certainly a challenge. We had some indication of the questions we wanted to ask before preparing the data. While this may have restricted our later exploration, we certainly still allowed the data to show us it's story rather than directly answer our hypothesis. Proving that quick exploration is very valuable before continuing analysis or building more complicated tools. 